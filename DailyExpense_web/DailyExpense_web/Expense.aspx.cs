﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace DailyExpense_web
{
    public partial class Expense : System.Web.UI.Page
    {
        SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPTOP-JQK15P7R;Initial Catalog=DB1;User ID=sa;Password=core044#");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnDelete.Enabled = false;
                //FillGridView();
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
        public void Clear()
        {
            hfItemID.Value = "";
            txtAdded.Text = txtDate.Text = txtItem.Text = txtPrice.Text = "";
            lblSuccessMessage.Text = lblErrorMessage.Text = "";
            btnSave.Text = "Save";
            btnDelete.Enabled = false;

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("ItemAddOrEdit_Web", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@Item_no",(hfItemID.Value==""?0:Convert.ToInt32(hfItemID.Value)));
            sqlCmd.Parameters.AddWithValue("@Item",txtItem.Text.Trim());
            sqlCmd.Parameters.AddWithValue("@Price", txtPrice.Text);
            sqlCmd.Parameters.AddWithValue("@Added_By",txtAdded.Text.Trim());
            sqlCmd.Parameters.AddWithValue("@Created_ON",Convert.ToDateTime(txtDate.Text));
           // sqlCmd.Parameters.AddWithValue("@Item_no", 0);
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();
            String ItemID = hfItemID.Value;
            Clear();
            if (ItemID == "")
                lblSuccessMessage.Text="Saved Successfully";
            else
                lblErrorMessage.Text="Updated Successfully";
            FillGridView();
           
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Calendar1.Visible = true;
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            txtDate.Text = Calendar1.SelectedDate.ToLongDateString();
            Calendar1.Visible = false;
        }
        void FillGridView()
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("ItemSearch_All",sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dtbl = new DataTable();
            sqlDa.Fill(dtbl);
           // sqlCon.Close();
            gvItem.DataSource = dtbl;
            gvItem.DataBind();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGridView();
        }
        protected void lnk_OnClick(object sender, EventArgs e)
        {
            int ItemNO = Convert.ToInt32((sender as LinkButton).CommandArgument);
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("ItemSearchByItemNO", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.Parameters.AddWithValue("@Item_no", ItemNO);
            DataTable dtbl = new DataTable();
            sqlDa.Fill(dtbl);
            hfItemID.Value = ItemNO.ToString();
            txtItem.Text = dtbl.Rows[0]["Item"].ToString();
            txtPrice.Text = dtbl.Rows[0]["Price"].ToString();
            txtAdded.Text = dtbl.Rows[0]["Added_By"].ToString();
            txtDate.Text= dtbl.Rows[0]["Created_ON"].ToString();
            
            btnSave.Text = "Update";
            btnDelete.Enabled = true;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("ItemDelete", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@Item_no", (hfItemID.Value == "" ? 0 : Convert.ToInt32(hfItemID.Value)));
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();
            String ItemID = hfItemID.Value;
            Clear();
            lblSuccessMessage.Text = "Deleted Successfully";
            FillGridView();
        }
    }
}