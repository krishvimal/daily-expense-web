﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Expense.aspx.cs" Inherits="DailyExpense_web.Expense" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="hfItemID" runat="server" />
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Item"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtItem" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Price"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="AddedBy"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtAdded" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Createdate"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">ChooseDate</asp:LinkButton>
                </td>
                <td>
                    <div>
                        <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged" Visible="False"></asp:Calendar>
                    </div>
                    
                </td>
               
            </tr>
            <tr>
                <td>
                    
                </td>
                <td colspan="2">
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" />
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" />
                </td>
            </tr>
            <tr>
                <td>
                    
                </td>
                <td colspan="2">
                    <asp:Label ID="lblSuccessMessage" runat="server" Text="" ForeColor="Green" Font-Bold></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    
                </td>
                <td colspan="2">
                    <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="Red" Font-Bold></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="Item" HeaderText="Item"/>
                <asp:BoundField DataField="Price" HeaderText="Price"/>
                <asp:BoundField DataField="Added_By" HeaderText="Added_By"/>
                <asp:BoundField DataField="Created_ON" HeaderText="Created_ON"/> 
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkView" runat="server" CommandArgument='<%# Eval("Item_no") %>' OnClick="lnk_OnClick">View</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>   
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
